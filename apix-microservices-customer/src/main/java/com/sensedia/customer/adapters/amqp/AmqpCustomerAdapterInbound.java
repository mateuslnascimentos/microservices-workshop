package com.sensedia.customer.adapters.amqp;

import com.sensedia.commons.errors.resolvers.ExceptionResolver;
import com.sensedia.customer.adapters.amqp.config.BindConfig;
import com.sensedia.customer.adapters.amqp.config.BrokerInput;
import com.sensedia.customer.adapters.dtos.CustomerCreationDto;
import com.sensedia.customer.adapters.mappers.CustomerMapper;
import com.sensedia.customer.domains.Customer;
import com.sensedia.customer.ports.AmqpPort;
import com.sensedia.customer.ports.ApplicationPort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(BrokerInput.class)
@Slf4j
public class AmqpCustomerAdapterInbound {

    private final ApplicationPort applicationPort;
    private final CustomerMapper customerMapper;
    private final AmqpPort amqpPort;
    private final ExceptionResolver exceptionResolver;

    public AmqpCustomerAdapterInbound(
            ApplicationPort applicationPort,
            CustomerMapper customerMapper, AmqpPort amqpPort, ExceptionResolver exceptionResolver) {
        this.applicationPort = applicationPort;
        this.customerMapper = customerMapper;
        this.amqpPort = amqpPort;
        this.exceptionResolver = exceptionResolver;
    }

    @StreamListener(target = BindConfig.SUBSCRIBE_CUSTOMER_CREATION_REQUESTED)
    public void subscribeExchangeCustomerCreationRequested(CustomerCreationDto customerCreationDto) {
        try {
            log.info("Customer received: " + customerCreationDto.toString());
            Customer customer = this.applicationPort.create(this.customerMapper.toCustomer(customerCreationDto));
            log.info("Customer created: " + customer.getId());
        } catch (Exception e) {
            log.error("Error in amqp port inbound customer creation requested: " + e.getMessage());
            amqpPort.publishCustomerOperationError(
                    exceptionResolver.solve(e).addOriginalMessage(customerCreationDto));
        }
    }
}
