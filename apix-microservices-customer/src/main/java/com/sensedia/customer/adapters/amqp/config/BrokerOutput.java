package com.sensedia.customer.adapters.amqp.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component
public interface BrokerOutput {

  @Output(BindConfig.PUBLISH_CUSTOMER_CREATED)
  MessageChannel publishCustomerCreated();

  @Output(BindConfig.PUBLISH_CUSTOMER_OPERATION_ERROR)
  MessageChannel publishCustomerOperationError();
}
