package ${package}.notification.adapters.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class CustomerAccountCreationDto {

  private CustomerCreationDto customer;

  private String number;

  private String agency;

  private BigDecimal availableCreditLimit;

}
