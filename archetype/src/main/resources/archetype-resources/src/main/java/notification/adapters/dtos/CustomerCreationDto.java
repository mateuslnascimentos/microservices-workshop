package ${package}.notification.adapters.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CustomerCreationDto {

    private String firstName;

    private String lastName;

}
